import csv
import logging
import os
import time
from datetime import datetime
from pathlib import Path
import paramiko
import httpx
import ast
import pprint
from pythonping import ping
import multiprocessing

ssh = paramiko.SSHClient()
ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())

error_list = []
log_list = []
warnings_misc = []
warnings_rest_api = []
warnings_firmware = []
warnings_model = []
warnings_cpu = []
warnings_appstate = []
warnings_calendar_error = []
warnings_sfb_line = []
warnings_ipt = []
warnings_offline = []
warnings_auth = []

unchanged_devices = []
configured_devices = []
warnings_networked_devices = []
warnings_missing_camera = []
object_devices = []


class Device:
    def __init__(self, asset_name, ip_address, make,
                 model, poly_epg, username, password):
        self.asset_name = asset_name
        self.ip_address = ip_address
        self.make = make
        self.model = model
        self.poly_epg = poly_epg
        self.username = username
        self.password = password


def log_sort_print(category, prompt):
    if category:
        category.sort()
        list_length = 0
        for item in category:
            if 'CIP_ID  Type    Status' in item:
                list_length += 1
        if list_length > 0:
            category.insert(0, f'{prompt} ({list_length}/{len(csv_device_list)})')
        else:
            category.insert(0, f'{prompt} ({len(category)}/{len(csv_device_list)})')
        print("\n".join(category))
        error_list.append(category)
        print('\n')


def init_logging():
    filename = Path(__file__).stem
    # logfilename = datetime.now().strftime(f'{filename}_%H_%M_%d_%m_%Y.log')
    logfilename = datetime.now().strftime(f'{filename}_%Y%m%d_%H-%M.log')
    pathandlogfilename = os.path.join(os.path.dirname(
        os.path.realpath(__file__)), 'logs', logfilename)

    logging.basicConfig(filename=f'{pathandlogfilename}', filemode='w',
                        format='%(message)s', level=logging.INFO)


def device_inst(asset_name, ip_address, make, model, poly_epg,
                username, password):
    obj_name = asset_name
    obj_name = Device(asset_name=asset_name,
                      ip_address=ip_address,
                      make=make,
                      model=model,
                      poly_epg=poly_epg,
                      username=username,
                      password=password,)
    object_devices.append(obj_name)


def csv_import():
    import tkinter as tk
    from tkinter import filedialog
    root = tk.Tk()
    root.withdraw()
    file_path = filedialog.askopenfilename()

    # Prompt for CSV, convert to list of dictionaries
    with open(file_path) as device_csv:
        csv_read = csv.DictReader(device_csv, delimiter=',')
        dict_devices = (list(csv_read))

        # Instantiate objects for devices
        for device in dict_devices:
            device_inst(device['Asset Name'],
                        device['IP Address'],
                        device['Make'],
                        device['Model'],
                        device['poly_epg'],
                        device['User'],
                        device['PW'])
    return object_devices


def print_append(text):
    print(text)
    log_list.append(str(text))
    time.sleep(.05)
    # logging.info(str(text))


def print_log(text):
    print(text)
    logging.info(str(text))


def main(device_list):
    from selenium import webdriver
    from selenium.webdriver.common.keys import Keys
    from selenium.webdriver.common.by import By
    from selenium.webdriver.support.ui import WebDriverWait
    from selenium.webdriver.chrome.options import Options
    from selenium.webdriver.support import expected_conditions as ec
    chrome_options = Options()
    chrome_options.add_argument('--ignore-certificate-errors')
    chrome_options.add_argument('--headless')
    chrome_options.add_argument('--window-size=1920,1080')
    chrome_options.add_experimental_option("excludeSwitches", ['enable-automation'])
    driver = webdriver.Chrome("C:/Windows/chromedriver.exe", options=chrome_options)

    def device_log_tag(text_input):
        log_tag = f'{device.asset_name}\t' \
                  f'{device.make} {device.model}\t' \
                  f'{datetime.now()}\t' \
                  f'{str(text_input)}'
        return log_tag

    def validate_firmware(firmware, min_firmware, validity):
        fw = str(firmware.replace("v", ""))
        fw_1 = fw[0: 1]  # x
        fw_2 = fw[2: 5]  # yyy
        fw_3 = fw[6: 10]  # zzzz
        min_fw = str(min_firmware)  # x.yyy.zzzz
        min_fw_1 = min_fw[0: 1]  # x
        min_fw_2 = min_fw[2: 5]  # yyy
        min_fw_3 = min_fw[6: 10]  # zzzz
        if int(fw_1) >= int(min_fw_1):
            if int(fw_2) >= int(min_fw_2):
                if int(fw_3) >= int(min_fw_3):
                    validity = True
        else:
            validity = False
        return validity

    def ssh_connect(device_name, device_ip, device_user, device_pw):
        print_append(f'{device_log_tag("Attempting to Connect via SSH")}')
        ssh.connect(device_ip, 22, device_user, device_pw)
        print_append(f'{device_log_tag("Connected")}')
        ssh.exec_command('')

    def ssh_push_cmd(command):
        if "Group" in device.model:
            try:
                channel = ssh.invoke_shell()
                channel.send("\n")
                channel.send(f"{command}\n")
                time.sleep(1)
                while not channel.recv_ready():
                    time.sleep(.25)
                out = channel.recv(9999).decode('utf-8')
                time.sleep(1)
                out = str(out).replace(f"{command}", '', 1)
                out = str(out).replace(f"\r", '')
                out = str(out).replace(f"-> ", '')
                response = out.strip()
                return response
            except Exception as e:
                print_append(f'{device_log_tag(e)}')

        else:
            stdin, stdout, stderr = ssh.exec_command(command)
            response = stdout.readlines()
            return response

    def ssh_parse_output(output, row, first, last):
        parsed_str = output[row]
        parsed_str = parsed_str.split(first)[1]
        parsed_str = parsed_str.split(last)[0]
        return parsed_str

    def rest_checkup(rest_model):

        def rest_get(path, print_state):
            response = httpx.get(f'{endpoint}{path}',
                                 verify=False, auth=(device.username, device.password),
                                 headers=headers, timeout=10.0)
            json_dict = ast.literal_eval(response.text)
            if print_state is True:
                pprint.pprint(json_dict)
            return json_dict

        endpoint = f'https://{device.ip_address}.micron.com'
        headers = {"Content-Type": "application/json",
                   'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like '
                                 'Gecko) Chrome/47.0.2526.111 Safari/537.36'}

        if rest_model == "PolyTrio":
            print_append(device_log_tag('Running REST API Calls'))

            try:
                # Check Skype for Business Registration
                device_dict = rest_get('/api/v2/mgmt/lineinfo', False)
                if device_dict['data'][0]['RegistrationStatus'] != 'Registered':
                    print_append(device_log_tag("Skype for Business Registration Error"))
                    warnings_sfb_line.append(f"{device_log_tag('')}: "
                                             f"{device_dict['data'][0]['RegistrationStatus']}")
                else:
                    print_append(device_log_tag("Skype for Business Registered"))

                # Check CPU Load
                device_dict = rest_get('/api/v1/mgmt/device/stats', False)
                cpu_load = float(device_dict['data']['CPU']['Average'])
                if cpu_load >= 95:
                    print_append(device_log_tag(f"High CPU Utilization: {cpu_load}%"))
                    warnings_cpu.append(f'{device_log_tag("")}: {cpu_load}%')
                else:
                    print_append(device_log_tag(f"Normal CPU Utilization: {cpu_load}%"))

                # Check AppState/FW
                device_dict = rest_get('/api/v2/mgmt/device/info', False)
                print_append(device_log_tag(f"App State: "
                                            f"{device_dict['data']['AppState']}"))
                print_append(device_log_tag(f"Firmware: "
                                            f"{device_dict['data']['Firmware']['Updater']}"))
                if device_dict['data']['AppState'] != 'AppStateMenu':
                    if device_dict['data']['AppState'] != 'AppStateCall':
                        warnings_appstate.append(device_log_tag(device_dict['AppState']))

                # Check DHCP
                device_dict = rest_get('/api/v1/mgmt/network/info', False)
                print_append(device_log_tag(f"IP Address: {device_dict['data']['IPV4Address']}"))
                if device_dict['data']['DHCP'] != 'enabled':
                    print_append(f'{device_log_tag("DHCP Not Enabled")}')
                    warnings_misc.append(f"{device_log_tag('DHCP Status: ')}"
                                         f"{device_dict['data']['DHCP']}")
                else:
                    print_append(f'{device_log_tag("DHCP Enabled")}')

            except Exception as e:
                print_append(device_log_tag('REST API Calls Failed: ' + str(e)))
                warnings_rest_api.append(device_log_tag(e))

    def ssh_checkup(ssh_model):

        try:
            ssh_connect(device.asset_name, device.ip_address, device.username, device.password)

            # Standard Crestron Checkup
            if ssh_model == "CrestronStandard":

                # Check firmware
                print_append(f'{device_log_tag("Checking Firmware Version")}')
                response_to_cmd = ssh_push_cmd('version')
                firmware = ssh_parse_output(response_to_cmd, 0, "[", " ")
                device.firmware = str(firmware)
                print_append(f'{device_log_tag("Firmware Version: ")}{device.firmware}')
                ssh.exec_command('')

                # Check CPU load
                if device.model != "TSS-7":
                    cpu_loads = []
                    print_append(f'{device_log_tag("Checking Average CPU Load")}')
                    for i in range(5):
                        response_to_cmd = ssh_push_cmd('cpuload')

                        # If device shows "Free CPU" instead of consumption % (TSS-7)
                        if 'Mem: ' in response_to_cmd[0]:
                            cpuload = ssh_parse_output(response_to_cmd, 1, " nic ", "% idle")[0:2]
                            cpuload = 100 - int(cpuload)
                            cpu_loads.append(int(cpuload))

                        # If device shows actual CPU load % (PRO3)
                        else:
                            cpuload = ssh_parse_output(response_to_cmd, 0, "LOAD ", "%")
                            cpu_loads.append(int(cpuload))

                    device.cpu_load = f"{sum(cpu_loads)/len(cpu_loads)}%"
                    print_append(f'{device_log_tag("Average CPU load: ")}'
                                 f'{device.cpu_load}')
                    avg_cpu_load = sum(cpu_loads)/len(cpu_loads)
                    if avg_cpu_load >= 97:
                        warnings_cpu.append(f'{device_log_tag("High CPU Consumption ")}'
                                            f'[{device.cpu_load}%]')

                # Check IP Table
                print_append(f'{device_log_tag("Checking IP Table")}')
                local_ipt_issues = []
                response_to_cmd = ssh_push_cmd('ipt')
                for line in response_to_cmd:
                    if line != '':
                        if 'IP Table' not in line:
                            if "  ONLINE  " not in line:
                                if "CONNECTED " not in line:
                                    if "127.000.000.001" not in line:
                                        line = line.replace('\r', '').replace('\n', '')
                                        local_ipt_issues.append(line)
                local_ipt_issues = local_ipt_issues[1:-1]
                if len(local_ipt_issues) >= 2:
                    print_append(f'{device_log_tag("Warnings Detailed Below:")}')
                    for issue in local_ipt_issues:
                        print_append(device_log_tag(issue))
                        warnings_ipt.append(device_log_tag(issue))
                else:
                    print_append(f'{device_log_tag("No Issues Found")}')

                # Close device iteration
                ssh.exec_command('bye')

            # Poly Group Checkup
            if ssh_model == "PolyGroup":

                # Check Calendaring
                print_append(f"{device_log_tag('Checking Calendar Status')}")
                response_to_cmd = ssh_push_cmd('calendarstatus get').split('\n')
                response_to_cmd = response_to_cmd[-1]
                if 'calendarstatus unavailable' in response_to_cmd:
                    print_append(device_log_tag('Exchange Not Logged In (' + response_to_cmd + ')'))
                    warnings_calendar_error.append(device_log_tag(f'{response_to_cmd}'))
                elif 'calendarstatus established' in response_to_cmd:
                    print_append(device_log_tag("Exchange Login Verified"))
                else:
                    print_append(device_log_tag(f"Unexpected Response: ({response_to_cmd})"))
                    warnings_misc.append(f"Unexpected Calendar Response: ({response_to_cmd})")

        except Exception as e:
            # Connection Error
            if "Errno 11001" in str(e):
                warnings_offline.append(device_log_tag(""))
                print_append(f'{device_log_tag("Device Unreachable")}')

            # Authentication Error
            elif "Authentication failed" in str(e):
                warnings_auth.append(device_log_tag(""))
                print_append(device_log_tag("Authentication Failed"))

            # General Exception
            else:
                print_append(device_log_tag(f'experienced an exception: {e}'))
                warnings_misc.append(f'{device_log_tag(e)}')

        finally:
            if ssh:
                ssh.close()
            print_append(f'{device_log_tag("Disconnected from Device")}')

    def web_driver_checkup(wd_model):

        #  Wait for XPATH to appear
        def expect_xpath(locator, timeout):
            wait = WebDriverWait(driver, timeout)
            model = wait.until(ec.presence_of_element_located((By.XPATH, locator)))
            while model.text == "":
                model = wait.until(ec.presence_of_element_located((By.XPATH, locator)))
                time.sleep(.5)
            return model.text

        #  Wait for CSS selector to appear
        def expect_css(locator, timeout):
            wait = WebDriverWait(driver, timeout)
            model = wait.until(ec.presence_of_element_located((By.CSS_SELECTOR, locator)))
            while model.text == "":
                model = wait.until(ec.presence_of_element_located((By.CSS_SELECTOR, locator)))
                time.sleep(.5)
            return model.text

        def wd_click(res_type, path):
            if 'css' in str(res_type):
                driver.find_element_by_css_selector(path).click()
            elif 'xpath' in res_type:
                driver.find_element_by_xpath(path).click()

        def wd_await_id(res_type, timeout, locator):
            try:
                if res_type == 'css':
                    wait = WebDriverWait(driver, timeout)
                    model = wait.until(ec.presence_of_element_located((By.CSS_SELECTOR, locator)))
                    while model.text == "":
                        model = wait.until(ec.presence_of_element_located((By.CSS_SELECTOR, locator)))
                        time.sleep(.5)
                    return model.text
                elif res_type == 'xpath':
                    wait = WebDriverWait(driver, timeout)
                    model = wait.until(ec.presence_of_element_located((By.XPATH, locator)))
                    while model.text == "":
                        model = wait.until(ec.presence_of_element_located((By.XPATH, locator)))
                        time.sleep(.5)
                    return model.text
            except Exception:
                return ''
                pass

        def nonawait_id(res_type, locator):
            if res_type == 'css':
                variable = remove(driver.find_element_by_css_selector
                                  (locator).get_attribute('innerHTML'))
            elif res_type == 'xpath':
                variable = remove(driver.find_element_by_xpath
                                  (locator).get_attribute('innerHTML'))
            variable = variable.replace('\n', '')
            variable = variable.replace('\r',  '')
            return variable

        def wd_confirm_and_click(conf_string, res_type, timeout, locator):
            if conf_string in wd_await_id(res_type, timeout, locator):
                wd_click(res_type, locator)
                return 'Pass'
            else:
                return 'Fail'

        #  Remove blanks from HTML strings
        def remove(string):
            return string.replace(" ", "")

        if wd_model == "PolyTrio":
            try:
                # Connect to device, wait for load
                print_append(f"{device_log_tag('Attempting to Connect to Webpage')}")
                driver.get(f'http://{device.ip_address}')
                print_append(f"{device_log_tag('Connected')}")

                # Attempt Login
                try:
                    wd_error = False
                    wd_await_id('xpath', 3, '/html/body/div[2]/table/tbody/tr/td/table/tbody/tr[1]/td/h1')
                except Exception:
                    try:
                        wd_confirm_and_click('here', 2, 'xpath', '/html/body/p/a')
                        try:
                            wd_await_id('xpath', 3, '/html/body/div[2]/table/tbody/tr/td/table/tbody/tr[1]/td/h1')
                            pass
                        except Exception:
                            try:
                                wd_confirm_and_click('here', 2, 'xpath', '/html/body/p/a')
                                wd_await_id('xpath', 3, '/html/body/div[2]/table/tbody/tr/td/table/tbody/tr[1]/td/h1')
                                pass
                            except Exception:
                                wd_confirm_and_click('Sign Out', 2, 'css', '#utility-nav > li:nth-child(2) > a > span')
                                wd_await_id('xpath', 3, '/html/body/div[2]/table/tbody/tr/td/table/tbody/tr[1]/td/h1')
                                pass
                    except Exception as e:
                        print_append(device_log_tag("Error, caught at login page"))
                        wd_error = True
                finally:
                    if not wd_error:
                        driver.find_element_by_xpath(
                            '/html/body/div[2]/table/tbody/tr/td/table/tbody/tr[3]/td/table/tbody/tr[4]/td[2]/input')\
                            .send_keys(f'{device.password}', Keys.ENTER)
                        print_append(f"{device_log_tag('Login Attempted')}")
                        wd_await_id('css', 15, '#topMenuItem5 > a > span')
                        print_append(f"{device_log_tag('Login Successful')}")

                # Check SFB Status
                if 'paired' not in device.poly_epg.casefold():
                    wd_confirm_and_click('Diagnostics', 'css', 15, '#topMenuItem5 > a > span')
                    if 'Fail' in wd_confirm_and_click('Skype for Business Status', 'css', 15, '#topMenuItem5 > ul > li:nth-child(3) > a > span'):
                        wd_confirm_and_click('Skype for Business Status', 'css', 15, '#topMenuItem5 > ul > li:nth-child(2) > a')
                        ews_status = wd_await_id('xpath', 3, '/html/body/div[5]/div[2]/div[6]/div[2]/div[1]/div/table/tbody/tr[1]/td[2]/span')
                        if ews_status == "EWS deployed":
                            print_append(f"{device_log_tag('EWS is Deployed')}")
                            device.ews_status = "Pass"
                        else:
                            print_append(device_log_tag(f"EWS Issue Detected: {ews_status}"))
                            warnings_calendar_error.append(device_log_tag(ews_status))
                            device.ews_status = "Fail"
                            # wd_confirm_and_click('Settings', 'css', 3, '#topMenuItem4 > a > span')
                            # wd_confirm_and_click('Skype for Business Sign In', 'css', 3, '#topMenuItem4 > ul > li:nth-child(22) > a > span')
                            # time.sleep(5)
                            # if 'Fail' in wd_confirm_and_click('Sign In', 'css', 3, '#lyncSignInBtn'):
                            #     print_append(device_log_tag(f"Device Still Logged In, Logging Out"))
                            #     wd_confirm_and_click('Sign Out', 'css', 1, '#lyncSignOutBtn')
                            #     wd_confirm_and_click('Sign In', 'css', 10, '#lyncSignInBtn')
                            #     '#popupdlg > div.popup-content'
                            #     if 'authentication failed' in wd_await_id('css', 10, '#popupdlg > div.popup-content'):
                            #         print_append(device_log_tag(f"Authentication Failed"))
                else:
                    print_append(device_log_tag('Paired Trio, Not Checking EWS Status'))

            # Check Camera
                wd_confirm_and_click('Diagnostics', 'css', 2, '#topMenuItem5 > a > span')
                if 'Fail' in wd_confirm_and_click('Networked Devices Status', 'css', 2, '#topMenuItem5 > ul > li:nth-child(2) > a > span'):
                    if 'audio only' not in device.poly_epg.casefold():
                        print_append(device_log_tag("No Networked Devices"))
                        warnings_networked_devices.append(device_log_tag("No Networked Devices"))
                    else:
                        print_append(device_log_tag("Audio Only Trio: Not Checking Networked Devices"))
                else:
                    wd_await_id('xpath', 5, '/html/body/div[5]/div[2]/div[6]/div[2]/h1/span')
                    print_append(device_log_tag("Checking Networked Devices"))
                    nd_ip = wd_await_id('xpath', 1, '/html/body/div[5]/div[2]/div[6]/div[2]/div/table/tbody/tr[2]/td[2]')
                    nd_mac = wd_await_id('xpath', 1, '/html/body/div[5]/div[2]/div[6]/div[2]/div/table/tbody/tr[1]/td[2]')
                    print_append(device_log_tag(f'Networked Device IP Address(s): {nd_ip}'))
                    print_append(device_log_tag(f'Networked Device MAC Address(s): {nd_mac}'))
                    if 'paired' not in device.poly_epg.casefold():
                        if 'audio only' not in device.poly_epg.casefold():
                            device.camera = nonawait_id('xpath', '/html/body/div[5]/div[2]/div[6]/div[2]/div/table/tbody/tr[3]/td[2]')
                            if device.camera != '':
                                print_append(device_log_tag(f'Camera Info: {device.camera}'))
                            else:
                                warnings_missing_camera.append(device_log_tag("No Camera Detected"))
                                print_append(device_log_tag('Camera Info: No Camera Detected'))
                                device.camera = 'None'

                # Sign Out
                wd_confirm_and_click('Log Out', 'css', 3, '#utility-nav > li:nth-child(2) > a > span')
                print_append(device_log_tag("Signing Out"))
                expect_xpath('/html/body/div[2]/table/tbody/tr/td/table/tbody/tr[1]/td/h1', 3)
                print_append(device_log_tag("Signed Out"))

            except Exception as e:
                print_append(device_log_tag(e))
                warnings_misc.append(device_log_tag(e))

        if wd_model == "CrestronTSS":

            # Validate Firmware
            firmware_valid = validate_firmware(device.firmware, '2.009.0080', False)
            if firmware_valid is True:
                print_append(device_log_tag('Firmware Validated, Attempting Web Scraping'))

                try:
                    # Connect to device, wait for load
                    print_append(device_log_tag('Attempting to Connect to Webpage'))
                    driver.get(f'http://{device.ip_address}')
                    expect_css('#credentials > button > span.ui-button-text.ui-c', 15)
                    print_append(device_log_tag('Success'))

                    # Find username field, enter username/PW/Enter
                    wd_click('xpath', '/html/body/div[1]/div/div[3]/div[1]/div[1]/form/div[1]/input')
                    driver.find_element_by_xpath(
                        '/html/body/div[1]/div/div[3]/div[1]/div[1]/form/div[1]/input') \
                        .send_keys(device.username, Keys.TAB, device.password, Keys.ENTER)
                    print_append(device_log_tag('Login Attempted'))

                    # Validate login
                    expect_xpath('/html/body/host-app/div[2]/div/div/header-detail/div/div/div/div/div[1]/h2/span', 60)
                    print_append(device_log_tag('Login Successful'))

                    # Validate model
                    if device.model in wd_await_id('xpath', 5, '/html/body/host-app/div[2]/div/div/div/div/div/div/helium-component/p-tabview/div/div/p-tabpanel[1]/div/device-status/p-accordion/div/product-component-proxy[1]/general-component/p-accordiontab/div[2]/div/div/div[1]/div[1]/div[2]'):

                        # Check Schedule
                        print_append(device_log_tag('Checking Scheduler Sync'))
                        wd_confirm_and_click('Schedule', 'xpath', 5, '/html/body/host-app/div[2]/div/div/div/div/div/div/helium-component/p-tabview/div/div/p-tabpanel[1]/div/device-status/p-accordion/div/p-accordiontab/div[1]/a/p-header/div/span')
                        calendar_state = wd_await_id('xpath', 10, '/html/body/host-app/div[2]/div/div/div/div/div/div/helium-component/p-tabview/div/div/p-tabpanel[1]/div/device-status/p-accordion/div/p-accordiontab/div[2]/div/div/div/div/product-component-proxy/status-schedule/form/div/div[2]/div[2]/span')
                        if 'Successfully connected' not in calendar_state:
                            print_append(device_log_tag(f'Device Calender Unsynced: "{calendar_state}"'))
                            warnings_calendar_error.append(device_log_tag(calendar_state))
                            device.calendar_sync = "Fail"
                        else:
                            print_append(device_log_tag(f'Device Calender Synced: "{calendar_state}"'))
                            device.calendar_sync = "Pass"
                    else:
                        print_append(device_log_tag(f'Model is incorrect'))
                        warnings_model.append(device_log_tag(f'Model is incorrect'))

                    # Logout
                    wd_click('xpath', '/html/body/host-app/div[1]/div/div[2]/a/i')
                    print_append(device_log_tag('Logging Out'))
                    wd_confirm_and_click('Sign Out', 'xpath', 5, '/html/body/host-app/p-overlaypanel/div/div/button/span')
                    if "Sign In" in wd_await_id('xpath', 8, '/html/body/div[1]/div/div[3]/div[1]/div[1]/form/button/span[2]'):
                        print_append(device_log_tag('Logout Successful'))

                except Exception as e:
                    device_log_tag(e)

    device_counter = 0

    for device in device_list:

        device_counter += 1
        print(f'(Device {device_counter} of {len(device_list)})')

        # Ping Test
        print_append(device_log_tag('Testing Ping'))
        ping_result = ''
        try:
            ping_result = str(ping(str(device.ip_address)))
        except Exception as e:
            print_append(device_log_tag('Ping Failed'))
            warnings_offline.append(device_log_tag(f'Ping Failed: {e}'))
        if 'Reply from ' in ping_result:
            print_append(device_log_tag(f'Beginning Audit {str(device.ip_address)}'))
            config_time_start = time.perf_counter()

            # Initiate checkups based on hardware
            if device.model == "Trio":
                if 'paired' not in device.poly_epg.casefold():
                    rest_checkup("PolyTrio")
                web_driver_checkup('PolyTrio')

            if "Group" in device.model:
                if device.make == "Poly":
                    ssh_checkup("PolyGroup")

            if device.make == "Crestron":
                ssh_checkup("CrestronStandard")
                if device.model == "TSS-7":
                    web_driver_checkup('CrestronTSS')

        else:
            ping_result = "Fail"

        # Close iteration
        if ping_result != "Fail":
            config_time_finish = time.perf_counter()
            config_time_total = f'({round(config_time_finish - config_time_start, 2)} seconds)'
            print_append(f"{device_log_tag('Device Iteration Closed')} {config_time_total}\n")
        else:
            print_append(f"{device_log_tag('Device Iteration Closed')}\n")


if __name__ == "__main__":

    #  Begin logging
    filename = Path(__file__).stem
    csvfilename = datetime.now().strftime(f'{filename}_%Y%m%d_%H-%M.csv')
    logfilename = datetime.now().strftime(f'{filename}_%Y%m%d_%H-%M.log')
    pathandlogfilename = os.path.join(os.path.dirname
                                      (os.path.realpath(__file__)), 'logs', logfilename)
    pathandcsvfilename = os.path.join(os.path.dirname
                                      (os.path.realpath(__file__)), 'logs', csvfilename)
    start_time = time.time()
    init_logging()

    #  Import CSV from prompt, convert to list of dictionaries
    print("Please select a raw CSV file to reference:\n\n")
    csv_device_list = csv_import()

    #  Verify successful import before running main function
    print(f'{len(csv_device_list)} item(s) imported.  Please confirm this looks correct:\n')
    counter = 0
    for obj_device in object_devices:
        if counter <= 2:
            print(f'{obj_device.asset_name.upper()}:\n'
                  f'IP Address/Hostname: {obj_device.ip_address}\n'
                  f'Make: {obj_device.make}\n'
                  f'Model: {obj_device.model}\n'
                  f'Endpoint Group: {obj_device.poly_epg}\n')
            counter += 1
    result = input('Type "Yes" to continue\n\n')
    if result == "yes".casefold():
        main(csv_device_list)

        # Summary log
        print_log('\n' * 4 + "Batch audit completed, summary below:\n\n")
        log_sort_print(warnings_cpu, "High CPU Usages:")
        log_sort_print(warnings_appstate, "Abnormal Appstate Errors:")
        log_sort_print(warnings_calendar_error, "Exchange/Calendar Errors:")
        log_sort_print(warnings_sfb_line, "SFB Line Registration Errors:")
        log_sort_print(warnings_misc, "Misc. Problem Devices:")
        log_sort_print(warnings_firmware, "Devices w/ Invalid Firmware:")
        log_sort_print(warnings_model, "Devices w/ Invalid Model:")
        log_sort_print(warnings_offline, "Unreachable Devices:")
        log_sort_print(warnings_auth, "Authentication Failures:")
        log_sort_print(warnings_ipt, "IP Table Warnings:")
        log_sort_print(warnings_networked_devices, "No Networked Devices:")
        log_sort_print(warnings_missing_camera, "Poly Devices Missing Cameras:")
        log_sort_print(warnings_rest_api, "API Call Failures:")

        while "" in log_list:
            log_list.remove("")
        log_list.sort()
        print("Event Log")
        print("\n".join(log_list))

        errors = []
        for categories in error_list:
            for line in categories:
                if '(Session info: headless chrome=85.0.4183.102)' not in line:
                    errors.append(line)
            errors.append('\n')

        logfilename = str(os.path.join(os.path.dirname(os.path.realpath(__file__)),
                                       'logs', logfilename))
        with open(logfilename, "w") as log_text:
            for line in errors:
                log_text.write(line + '\n')
            log_text.write(f"Event Log: ({len(csv_device_list)} Devices)\n")
            for line in log_list:
                log_text.write(line + '\n')
        log_text.close()


